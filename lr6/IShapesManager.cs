﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr6
{
    interface IShapesManager
    {
        Settings getSettings();
        void setSettings(Settings s);
        void addShape(int x, int y);
        void selectShape(int x, int y);
        void deleteSelectedShape();
        IStore<IShape> getShapes();

        void shift(int dx, int dy);
        void subscribe(EventHandler handler);
        void unsubscribe(EventHandler handler);
    }
}
