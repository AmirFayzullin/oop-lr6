﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr6
{
    struct Settings
    {
        public ShapesTypes shapeType;
        public Colors color;
        public float scaling;
        public Settings(ShapesTypes st, Colors c, float s)
        {
            color = c;
            scaling = s;
            shapeType = st;
        }

        private Settings(Settings copyFrom)
        {
            color = copyFrom.color;
            scaling = copyFrom.scaling;
            shapeType = copyFrom.shapeType;
        }

        public Settings copy() { return new Settings(this); }
    }

    struct CanvasDimensions
    {
        public readonly int x0;
        public readonly int y0;
        public readonly int x1;
        public readonly int y1;
        
        public CanvasDimensions(int _x0, int _y0, int _x1, int _y1)
        {
            x0 = _x0;
            y0 = _y0;
            x1 = _x1;
            y1 = _y1;
        }

        public CanvasDimensions(CanvasDimensions c)
        {
            x0 = c.x0;
            y0 = c.x0;
            x1 = c.x1;
            y1 = c.y1;
        }
    }
    class CShapesManager : IShapesManager
    {
        delegate void storeWalkerCb(IShape shape);
        public const int DX = 10;
        public const int DY = 10;
        readonly CanvasDimensions dimensions;

        IStore<IShape> shapes = new CStore<IShape>();
        Settings settings;
        EventHandler observers;


        public CShapesManager(CanvasDimensions d) {
            settings = new Settings(ShapesTypes.Circle, Colors.Red, 1);
            dimensions = new CanvasDimensions(d);
        }

        public Settings getSettings() { return settings.copy(); }

        public void setSettings(Settings s) {
            if (s.scaling <= 0)
            {
                notifyObservsers();
                return;
            }
            settings = s.copy();
            IShape selected = getSelectedShape();

            if (selected == null) {
                notifyObservsers();
                return;
            };
            selected.setScale(settings.scaling);
            selected.setColor(settings.color);
            notifyObservsers();
        }

        public void shift(int dx, int dy)
        {
            IShape selected = getSelectedShape();
            if (selected == null) return;
            selected.shift(dx, dy);
            notifyObservsers();
        }

        public void addShape(int x, int y)
        {
            IShape shape = null;
            switch(settings.shapeType)
            {
                case ShapesTypes.Circle:
                    if (CCircle.isOutOfBox(x, y, dimensions, settings.scaling)) break;

                    shape = new CCircle(x, y, settings.color, dimensions);
                    shape.setScale(settings.scaling);

                    break;
                case ShapesTypes.Square:
                    if (CSquare.isOutOfBox(x, y, dimensions, settings.scaling)) break;

                    shape = new CSquare(x, y, settings.color, dimensions);
                    shape.setScale(settings.scaling);
                    
                    break;
            }

            if (shape == null) return;

            shapes.Add(shape);
            notifyObservsers();
        }

        public void selectShape(int x, int y)
        {
            IShape alreadySelected = getSelectedShape();
            IShape shape = shapeAt(x, y);
            if (shape == null || shape == alreadySelected)
            {
                removeSelection();
                notifyObservsers();
                return;
            } else if (shape != null && shape != alreadySelected)
            {
                removeSelection();
            }

            shape.select(!shape.isSelected());

            settings.scaling = shape.getScale();
            settings.color = shape.getColor();
            
            notifyObservsers();
        }

        public void deleteSelectedShape()
        {
            IShape selected = getSelectedShape();
            if (selected == null) return;

            goViaStore((IShape s) =>
            {
                if (s == selected) shapes.RemoveCurrent();
            });

            notifyObservsers();
        }

        public IStore<IShape> getShapes() { return shapes; }

        public void subscribe(EventHandler handler) { observers += handler; }
        public void unsubscribe(EventHandler handler) { observers -= handler; }


        private IShape shapeAt(int x, int y)
        {
            IShape shape = null;

            goViaStore((IShape s) =>
            {
                if (s.hittest(x, y)) shape = s;
            });

            return shape;
        }

        private IShape getSelectedShape()
        {
            IShape selected = null;

            goViaStore((IShape shape) =>
            {
                if (shape.isSelected()) selected = shape;
            });

            return selected;
        }

        private void goViaStore(storeWalkerCb cb)
        {
            for (shapes.First(); !shapes.IsEol(); shapes.Next())
                cb(shapes.GetCurrent());
        }

        private void notifyObservsers()
        {
            if (observers == null) return;
            observers.Invoke(this, null);
        }

        private void removeSelection()
        {
            goViaStore((IShape s) =>
            {
                if (s.isSelected()) s.select(false);
            });
        }
    }
}
