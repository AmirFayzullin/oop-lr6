﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lr6
{
    enum Colors { Red, Green, Blue }
    enum ShapesTypes { Circle, Square }
    interface IShape
    {
        bool isSelected();
        void select(bool toSelect);

        Colors getColor();
        float getScale();
        void setColor(Colors color);

        void setScale(float k);
        void shift(int dx, int dy);

        bool hittest(int _x, int _y);
        void render(Graphics g);
    }
}
