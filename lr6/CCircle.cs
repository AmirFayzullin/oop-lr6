﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lr6
{
    class CCircle : CShape, IShape
    {
        const int initialR = 20;
        int r = initialR;

        public CCircle(int _x, int _y, Colors c, CanvasDimensions d) : base(_x, _y, c, d)
        {
        }

        override public void setScale(float k) {
            if (isOutOfBox(x, y, dimensions, k)) return;
            base.setScale(k);
            r = Convert.ToInt32(initialR * k);
        }        

        override public bool hittest(int _x, int _y)
        {
            return Math.Sqrt(Math.Pow(Math.Abs(x - _x), 2) + Math.Pow(Math.Abs(y - _y), 2)) <= r;
        }

        public override void shift(int dx, int dy)
        {

            if (isOutOfBox(x + dx, y + dy, dimensions, scaling)) return;
            base.shift(dx, dy);
        }

        public static bool isOutOfBox(int x, int y, CanvasDimensions d, float scaling)
        {
            int r = (int)(initialR * scaling);

            return x + r > d.x1 ||
                    x - r < d.x0 ||
                    y + r > d.y1 ||
                    y - r < d.y0;
        }

        override public void render(Graphics g)
        {
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            SolidBrush b = new SolidBrush(mapToColor());

            g.FillEllipse(b, new Rectangle(x - r, y - r, r * 2, r * 2));

            if (selected)
            {
                Pen p = new Pen(Color.Black);
                g.DrawRectangle(p, new Rectangle(x - r - 10, y - r - 10, r * 2 + 20, r * 2 + 20));
                p.Dispose();
            }

            b.Dispose();
        }
    }
}
