﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr6
{
	class Item<T>
	{
		public T value;
		public Item<T> prev;
		public Item<T> next;

		public Item(T _value, Item<T> _prev = null, Item<T> _next = null)
		{
			value = _value;
			prev = _prev;
			next = _next;
		}
	}
	class CStore<T> : IStore<T>
	{
		Item<T> _current;
		Item<T> _first;
		Item<T> _last;
		int count = 0;

		public int Count
		{
			get { return count; }
		}

		public CStore()
		{
			_current = null;
			_first = null;
			_last = null;
		}

		public void Add(T value)
		{
			Item<T> item = new Item<T>(value);
			if (_first == null)
			{
				// when it's first item
				_first = item;
				_last = item;
				_current = item;
			}
			else
			{
				// pushing to the end
				_last.next = item;
				item.prev = _last;
				_last = item;
			}
			count++;
		}

		public T RemoveCurrent()
		{
			if (_current == null) return default(T);
			Item<T> prev = _current.prev;
			Item<T> next = _current.next;
			T value = _current.value;

			if (prev == null && next == null)
			{
				// if deleted element is only one in list
				_first = null;
				_last = null;
				_current = null;
			}
			else if (next == null)
			{
				// if it's last
				_last = prev;
				_last.next = null;
				_current = prev;
			}
			else if (prev == null)
			{
				// if it's first
				_first = next;
				_first.prev = null;
				_current = next;
			}
			else
			{
				// if it's in the middle
				prev.next = next;
				next.prev = prev;
				_current = next;
			}
			count--;
			return value;
		}

		public void First()
		{
			_current = _first;
		}

		public void Last()
		{
			_current = _last;
		}

		public void Next()
		{
			if (!IsEol()) _current = _current.next;
		}

		public void Prev()
		{
			if (!IsEol()) _current = _current.prev;
		}

		public bool IsEol()
		{
			return _current == null;
		}

		public T GetCurrent()
		{
			return _current.value;
		}

		public bool Contains(cmpFn<T> fn)
		{
			First();
			while (!IsEol())
			{
				if (!fn(_current.value)) { Next(); continue; }
				return true;
			}
			First();
			return false;
		}

		~CStore()
		{
			if (_first == null) return;
			First();

			while (_current != null)
			{
				Item<T> next = _current.next;
				_current.next = null;
				_current.prev = null;
				_current = next;
			}
		}
	}
}
