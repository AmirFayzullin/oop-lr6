﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr6
{
    class CSquare : CShape, IShape
    {
        const float initialHalfWidth = 20;
        float halfWidth = initialHalfWidth;

        public CSquare(int x, int y, Colors c, CanvasDimensions d) : base(x, y, c, d) { }

        public override void setScale(float k)
        {
            if (isOutOfBox(x, y, dimensions, k)) return;
            base.setScale(k);
            halfWidth = initialHalfWidth * k;
        }

        public override void shift(int dx, int dy)
        {

            if (isOutOfBox(x + dx, y + dy, dimensions, scaling)) return;
            base.shift(dx, dy);
        }

        public override void render(Graphics g)
        {
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            SolidBrush b = new SolidBrush(mapToColor());

            g.FillRectangle(b, new Rectangle(x - (int)halfWidth, y - (int)halfWidth, (int)halfWidth * 2, (int)halfWidth * 2));

            if (selected)
            {
                Pen p = new Pen(Color.Black);
                g.DrawRectangle(p, new Rectangle(x - (int)halfWidth - 10, y - (int)halfWidth - 10, (int)halfWidth * 2 + 20, (int)halfWidth * 2 + 20));
                p.Dispose();
            }

            b.Dispose();
        }

        public static bool isOutOfBox(int x, int y, CanvasDimensions d, float scaling)
        {
            int hw = (int)(initialHalfWidth * scaling);
            return x + hw > d.x1 ||
                    x - hw < d.x0 ||
                    y + hw > d.y1 ||
                    y - hw < d.y0;
        }

        public override bool hittest(int _x, int _y)
        {
            int hw = (int)(halfWidth * scaling);

            return _x <= x + hw &&
                    _x >= x - hw &&
                    _y >= y - hw &&
                    _y <= y + hw;
        }
    }
}
