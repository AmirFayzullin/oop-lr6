﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lr6
{
    abstract class CShape : IShape
    {
        protected int x, y;
        protected float scaling = 1;
        protected Colors color;
        protected bool selected = false;
        protected readonly CanvasDimensions dimensions;

        protected CShape(int _x, int _y, Colors c, CanvasDimensions d)
        {
            x = _x;
            y = _y;
            color = c;
            dimensions = d;
        }

        virtual public bool isSelected() { return selected; }

        virtual public void select(bool toSelect) { selected = toSelect; }

        virtual public void setColor(Colors newColor) { color = newColor; }
        virtual public Colors getColor() { return color; }
        virtual public float getScale() { return scaling; }

        virtual public void setScale(float k)
        {
            scaling = k;
        }

        virtual public void shift(int dx, int dy) { x += dx; y += dy; }

        abstract public bool hittest(int x, int y);

        abstract public void render(Graphics g);

        protected Color mapToColor()
        {
            Color c;
            switch (color)
            {
                case Colors.Red:
                    c = Color.Red;
                    break;
                case Colors.Green:
                    c = Color.Green;
                    break;
                case Colors.Blue:
                    c = Color.Blue;
                    break;
                default:
                    c = Color.Red;
                    break;
            }

            return c;
        }
    }
}
