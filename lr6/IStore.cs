﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr6
{
	delegate bool cmpFn<T>(T item);
	interface IStore<T>
	{
		void Add(T value);
		T GetCurrent();
		T RemoveCurrent();
		void First();
		void Next();
		void Last();
		void Prev();
		bool IsEol();
		bool Contains(cmpFn<T> cmpcb);
	}
}
